insert into ExpDates values ('1', '365');

insert into MatTypes values ('1', 'Input material');
insert into MatTypes values ('2', 'Intermediate');
insert into MatTypes values ('3', 'Product');

insert into Units values ('1','mg');
insert into Units values ('2','g');
insert into Units values ('3','kg');
insert into Units values ('4','l');
insert into Units values ('5','item');

insert into Emplacements values ('1','510-VRZ', '3-8 �C');
insert into Emplacements values ('2','511-VRZ', '3-8 �C');
insert into Emplacements values ('3','222', '15-30 �C');

insert into Suppliers values ('1','BRENNTAG', 'ABCD', '2', '100');

insert into MaterialNos values ('1','50800', STR_TO_DATE('30-01-2014', '%d-%m-%Y'), '50800-SPC-01', '1', '1', '4','3','1');

insert into States values ('1','For re-anaysis');
insert into States values ('2','Quarantene');
insert into States values ('3','Approved');
insert into States values ('4','Disapproved');
insert into States values ('5','Disposed');
insert into States values ('6','Used');

insert into TranTypes values ('1', 'Reclamation');
insert into TranTypes values ('2', 'Quarantining');
insert into TranTypes values ('3', 'Consumption');
insert into TranTypes values ('4', 'Approval');
insert into TranTypes values ('5', 'Disposal');
insert into TranTypes values ('6', 'Configuration');

insert into `Groups` values ('1','ADM');
insert into `Groups` values ('2','QC/QA');
insert into `Groups` values ('3','PRO');

insert into Rights values ('1', 'Right to configurate');
insert into Rights values ('2', 'Right to reclamate');
insert into Rights values ('3', 'Right to quarantine');
insert into Rights values ('4', 'Right to withdraw');
insert into Rights values ('5', 'Right to approve');
insert into Rights values ('6', 'Right to dispose');

insert into Operators values ('1','Juraj Skvarla','js','a');
insert into Operators values ('2','Zdenek Jurka','zj','a');
insert into Operators values ('3','Dusan Klasovity','dk','a');
insert into Operators values ('4','Robin Fucik','rf','a');
insert into Operators values ('5','Vera Cerna','vc','a');
insert into Operators values ('6','Ondrej Kettner','ok','a');

insert into have_groups values ('1','4');
insert into have_groups values ('2','1');
insert into have_groups values ('2','2');
insert into have_groups values ('2','3');
insert into have_groups values ('3','5');
insert into have_groups values ('3','6');

insert into has_rights values ('1','2');
insert into has_rights values ('2','2');
insert into has_rights values ('5','2');
insert into has_rights values ('6','2');
insert into has_rights values ('4','3');
insert into has_rights values ('2','1');
insert into has_rights values ('3','1');

insert into Consumptions values ('Lecireline', '10');
insert into Consumptions values ('Dephereline', '10');
